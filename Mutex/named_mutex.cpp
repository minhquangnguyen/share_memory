/*
Named objects are managed by the operating system, are not stored in the shared memory, and can be referenced from programs by name.
*/

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <iostream>

using namespace boost::interprocess;

int main()
{
	//shared_memory_object::remove("shm");
	managed_shared_memory managed_shm{open_or_create, "shm", 1024};
	int *i = managed_shm.find_or_construct<int>("Integer")();
	named_mutex named_mtx{open_or_create, "mtx"};
	named_mtx.lock();
	++(*i);
	std::cout << *i << '\n';
	named_mtx.unlock();
	return 0;
}

//g++ -Wall -I D:/boost_1_79_0/ named_mutex.cpp -o named_mutex