#include <boost/interprocess/managed_shared_memory.hpp>
#include <cstring>
// #include <string>
#include <cstdlib> //std::system
#include <iostream>
#include <stdio.h>
#include <iostream>
#include <boost/interprocess/containers/string.hpp>

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


using namespace boost::interprocess;

class MyClass {
public:
	MyClass();
	MyClass(int, string);
	~MyClass();
	int id;
	boost::interprocess::string value;
};

MyClass::MyClass() { 
	this->id = 0; 
	this->value = "init";
}

MyClass::MyClass(int i, string v) { 
	this->id = i; 
	this->value = v;
}

MyClass::~MyClass() {
} 

int main( int argc, char *argv[] ) 
{
	srand (time(NULL));
	std::cout << "parent: " << std::endl;
	
	shared_memory_object::remove("MySharedMemory");
	
	managed_shared_memory shm(create_only, "MySharedMemory", 1000);
	
	MyClass *myclass = shm.construct<MyClass>("MyClass")(); // Construct a named object
	
	std::cout << "parent see: " << myclass->id << ": " << myclass->value << std::endl;
	while(true)
	{
		/* generate secret number between 1 and 10: */
		myclass->id = rand() % 10 + 1;

		std::cout << "parent see: " << myclass->id << ": " << myclass->value << std::endl;
	}
	
	return 0;		
}

// g++ -Wall -I D:/boost_1_79_0/ server.cpp -o server