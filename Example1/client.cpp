#include <boost/interprocess/managed_shared_memory.hpp>
#include <cstring>
//#include <string>
#include <cstdlib> //std::system
#include <iostream>
#include <stdio.h>
#include <iostream>
#include <boost/interprocess/containers/string.hpp>

using namespace boost::interprocess;

class MyClass {
public:
	MyClass();
	MyClass(int, string);
	~MyClass();
	int id;
	boost::interprocess::string value;
};

MyClass::MyClass() { 
	this->id = 0; 
	this->value = "init";
}

MyClass::MyClass(int i, string v) { 
	this->id = i; 
	this->value = v;
}

MyClass::~MyClass() {
}

int main( int argc, char *argv[] ) 
{

	std::cout << "child: " << std::endl;
	managed_shared_memory shm(open_only, "MySharedMemory");
	while(true)
	{
		MyClass *myclass = shm.find<MyClass>("MyClass").first;
		//MyClass mc = shm.find<MyClass>("MyClass").second;
		std::cout << shm.find<MyClass>("MyClass").second << std::endl;
		std::cout << "child see: " << myclass->id << ": " << myclass->value << std::endl;
		//myclass->value = "child make it better";
	}
	
	return 0;

}

// g++ -Wall -I D:/boost_1_79_0/ client.cpp -o client