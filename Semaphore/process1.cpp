#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <iostream>
#include <boost/interprocess/containers/string.hpp>
#include "doc_anonymous_semaphore_shared_data.hpp"

using namespace boost::interprocess;

int main ()
{
   try{
      //Erase previous shared memory
      shared_memory_object::remove("shared_memory");

      //Create a shared memory object.
      shared_memory_object shm
         (create_only                  //only create
         ,"shared_memory"              //name
         ,read_write  //read-write mode
         );

      //Set size
      shm.truncate(sizeof(shared_memory_buffer));

      //Map the whole shared memory in this process
      mapped_region region
         (shm                       //What to map
         ,read_write //Map it as read-write
         );

      //Get the address of the mapped region
      void * addr       = region.get_address();

      //Construct the shared structure in memory
      shared_memory_buffer * data = new (addr) shared_memory_buffer;

      const int NumMsg = 100;

		std::string strTemp;
		
      //Insert data in the array
      for(int i = 0; true; ++i){

         //data->mutex.wait();
         data->items[i % shared_memory_buffer::NumItems] = i;
		 
		 std::cout << "Nhap mot xau ky tu: " << std::endl;
		 
		 getline(std::cin, strTemp);
         data->value = strTemp;
		if(strTemp == "quit")
			break;
			
         data->mutex.post();
      }
   }
   catch(interprocess_exception &ex){
      shared_memory_object::remove("shared_memory");
      std::cout << ex.what() << std::endl;
      return 1;
   }

   //Erase shared memory
   shared_memory_object::remove("shared_memory");

   return 0;
}

// // g++ -Wall -I D:/boost_1_79_0/ process1.cpp -o proc1