#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <cstring>
#include <cstdlib>
#include <string>
#include <iostream>
using namespace std;
using namespace boost::interprocess;

/*struct shm_remove
{
	shm_remove() { shared_memory_object::remove("MySharedMemory"); }
	~shm_remove() { shared_memory_object::remove("MySharedMemory"); }
} remover;*/


int main(int argc, char *argv[])
{

	shared_memory_object::remove("MySharedMemory1");
	//Create a shared memory object.
	shared_memory_object shm (create_only, "MySharedMemory1", read_write);

	
	//Set size
	shm.truncate(1000);

	//Map the whole shared memory in this process
	mapped_region region(shm, read_write);
	
	
	cout << "Size: " << region.get_address() << endl;
	

	//Write all the memory to 1
	std::memcpy(region.get_address(), argv[1], strlen(argv[1]));
	
	char *mem = static_cast<char*>(region.get_address());
	
	mem[strlen(argv[1])] = 0;

	cout << "End." << endl;
	

	return 0;
}

// // g++ -Wall -I D:/boost_1_79_0/ serverSide.cpp -o server