#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <cstring>
#include <cstdlib>
#include <string>
#include <iostream>
using namespace std;


int main(int argc, char *argv[])
{
	using namespace boost::interprocess;

	//Open already created shared memory object.
	shared_memory_object shm (open_or_create, "MySharedMemory1", read_only);

	cout << "Here" << endl;
	//Map the whole shared memory in this process
	mapped_region region(shm, read_only);
	
	cout << "Here 1" << endl;

	cout << region.get_size() << endl;

	char *mem = static_cast<char*>(region.get_address());
	for(std::size_t i = 0; i < region.get_size(); ++i)
	{
		char c = (char)(*mem++);
		cout << (char) c << endl;
		if(c == 0)
			return 0;    		

	}
	shared_memory_object::remove("MySharedMemory1");
	return 0;
}

// g++ -Wall -I D:/boost_1_79_0/ clientSide.cpp -o client